package ictgradschool.web.lab14.ex02;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.Scanner;

public class RegisterUser extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet");
        HttpSession sess = req.getSession(true);

        ServletContext servletContext = getServletContext();
        String fullJsonPath = servletContext.getRealPath("/");
        // Read JSON file
        File jsonFile = new File(fullJsonPath+ String.valueOf(sess.getAttribute("userID")) + ".json");
        try (Scanner fileReader = new Scanner(jsonFile)) {
            while (fileReader.hasNext()) {
                JSONObject json = (JSONObject) JSONValue.parse(fileReader.next());
                req.setAttribute("submittedData", json);
            }
        }

        req.getRequestDispatcher("/register.jsp").forward(req, resp);
    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Retrieve an existing session or create a new one if needed
        HttpSession sess = req.getSession(true);
        // Loop over all keys in the session

        System.out.println("sessions:");
        Enumeration<String> attrKeys = sess.getAttributeNames();
        while (attrKeys.hasMoreElements()) {
            String key = attrKeys.nextElement();
            Object sessionValue = sess.getAttribute(key);
            System.out.println(sessionValue);
        }

        // Add a new object to the session
        sess.setAttribute("userID", sess.getId());


        // Create JSON
        JSONObject jsonObject = new JSONObject();
        Map<String, String[]> formData = req.getParameterMap();

        for (Map.Entry<String, String[]> entry : formData.entrySet()){
            System.out.println(entry.getKey());
            System.out.println(Arrays.toString(entry.getValue()));
            jsonObject.put(entry.getKey(), Arrays.toString(entry.getValue()));
        }
        ServletContext servletContext = getServletContext();
        String fullJsonPath = servletContext.getRealPath("/");

        // Write JSON file
        File jsonFile = new File(fullJsonPath+ String.valueOf(sess.getAttribute("userID")) + ".json");
        jsonFile.getParentFile().mkdirs();
        jsonFile.createNewFile();

        try (PrintWriter fileWriter = new PrintWriter(jsonFile)) {
            fileWriter.write(jsonObject.toJSONString());
        }
        System.out.println(jsonObject.toJSONString());

        // Read JSON file
        try (Scanner fileReader = new Scanner(jsonFile)) {
            while (fileReader.hasNext()) {
                JSONObject json = (JSONObject) JSONValue.parse(fileReader.next());
                req.setAttribute("fname", json.get("fname"));
                req.setAttribute("lname", json.get("lname"));
                req.setAttribute("email", json.get("email"));
                req.setAttribute("submittedData", json);
            }
        }

        req.getRequestDispatcher("/register.jsp").forward(req, resp);
    }
}

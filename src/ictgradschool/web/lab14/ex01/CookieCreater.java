package ictgradschool.web.lab14.ex01;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class CookieCreater extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        System.out.println(cookies);
        Cookie cookie = new Cookie("submitted", "0");
        resp.addCookie(cookie);
        int numSubmitted = Integer.parseInt((String)Arrays.stream(cookies).filter(c -> c.getName().equals("submitted")).findFirst().get().getValue());
        numSubmitted++;
        resp.addCookie(new Cookie("submitted", String.valueOf(numSubmitted)));
        resp.sendRedirect("/index.jsp");

    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

<%--
  Created by IntelliJ IDEA.
  User: qd16
  Date: 11/01/2018
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <div class="container">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius eveniet, harum iure libero magni quia quibusdam. Ab accusantium blanditiis deserunt distinctio dolorem eos esse iste non ratione, recusandae rem tempore.</p>
        <form action="/ex02/register" method="post">

            <label for="fname">First Name: </label>
            <input type="text" name="fname" id="fname">
            <br>
            <label for="lname">Last Name: </label>
            <input type="text" name="lname" id="lname">
            <br>
            <label for="email">Email:</label>
            <input type="email" name="email" id="email">
            <br>
            <input type="submit" name="submit" id="submit">
        </form>

        <p>
            ${submittedData}
        </p>
    </div>
</body>
</html>
